<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\V1\UserController;
use App\Http\Controllers\V1\AuthController;
use App\Http\Controllers\V1\RoleController;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\V1\PermissionController;

Route::group(['namespace' => 'V1', 'prefix' => '/v1'], function () {
    Route::group(['prefix' => '/auth'], function () {
        Route::get('/logout', [AuthController::class, 'logout']);
        Route::post('/login', [AuthController::class, 'login']);

        Route::group(['prefix' => '/verify'], function () {
            Route::get('/email/{token}', [AuthController::class, 'emailVerify']);
            Route::post('/email/resend', [AuthController::class, 'emailVerifyResend']);
        });

        Route::group(['prefix' => '/password'], function () {
            Route::post('/request-new-password', [AuthController::class, 'requestNewPassword']);
            Route::post('/create-new-password', [AuthController::class, 'createNewPassword']);
        });
    });

    Route::group(['middleware' => ['check.access.token', 'jwt.auth']], function () {
        Route::group(['prefix' => '/auth'], function () {
            Route::get('/me', [AuthController::class, 'me']);
            Route::post('/change-password', [AuthController::class, 'changePassword']);
        });

        Route::group(['prefix' => '/permission'], function () {
            Route::get('/', [PermissionController::class, 'index']);
            Route::get('/me', [PermissionController::class, 'permissionsByUser']);
        });

        Route::group(['prefix' => '/user'], function () {
            Route::get('/', [UserController::class, 'index']);
            Route::get('/{id}', [UserController::class, 'show']);

            Route::post('/', [UserController::class, 'store']);

            Route::put('/{id}', [UserController::class, 'update']);

            Route::delete('/{id}', [UserController::class, 'destroy']);
        });

        Route::group(['prefix' => '/role'], function () {
            Route::get('/', [RoleController::class, 'index']);
        });
    });
});

Route::fallback(function () {
    return response()->json([
        'project' => getenv('APP_NAME'),
    ], Response::HTTP_BAD_REQUEST);
});
