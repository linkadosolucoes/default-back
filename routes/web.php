<?php

use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

Route::fallback(function () {
    return response()->json([
        'project' => getenv('APP_NAME'),
    ], Response::HTTP_OK);
});
