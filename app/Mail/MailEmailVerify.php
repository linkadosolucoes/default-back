<?php

namespace App\Mail;

use App\Models\User;
use App\Models\EmailVerify;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailEmailVerify extends Mailable
{
    use Queueable, SerializesModels;

    private $user;

    private $emailVerify;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, EmailVerify $emailVerify)
    {
        $this->user = $user;

        $this->emailVerify = $emailVerify;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email-verify')
            ->subject('Verificação de E-mail')
            ->with([
                'name' => $this->user->name,
                'token' => $this->emailVerify->token
            ]);
    }
}
