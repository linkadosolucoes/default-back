<?php

namespace App\Mail;

use App\Models\User;
use App\Models\PasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailRequestNewpassword extends Mailable
{
    use Queueable, SerializesModels;

    private $user;

    private $passwordReset;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, PasswordReset $passwordReset)
    {
        $this->user = $user;

        $this->passwordReset = $passwordReset;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email-request-new-password')
            ->subject('Solicitação - Nova Senha')
            ->with([
                'name' => $this->user->name,
                'token' => $this->passwordReset->token
            ]);
    }
}
