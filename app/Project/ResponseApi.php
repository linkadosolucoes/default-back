<?php

namespace App\Project;

class ResponseApi
{
    static function success($data = [], $message = "Ação realizada com sucesso", $httpCode = 200)
    {
        return response()->json([
            'data' => $data,
            'message' => $message
        ], $httpCode);
    }

    static function error($error = null,  $message = "Não foi possível executar essa operação", $httpCode = 400)
    {
        return response()->json([
            'error' => $error,
            'message' => $message
        ], $httpCode);
    }

    static function errorServer($error = null,  $message = "Não foi possível executar essa operação", $httpCode = 500)
    {
        return response()->json([
            'error' => $error,
            'message' => $message
        ], $httpCode);
    }
}
