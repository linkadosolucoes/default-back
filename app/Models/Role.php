<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name',
        'description',
        'object'
    ];

    protected $with = [
        'permissions'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class);
    }

    public function permissions()
    {
        return $this->hasMany(\App\Models\Permission::class);
    }
}
