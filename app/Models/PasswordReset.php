<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'token',
        'revoged',
        'created_at'
    ];

    protected $casts = [
        'revoged' => 'boolean',
        'created_at' => 'datetime'
    ];
}
