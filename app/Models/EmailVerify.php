<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailVerify extends Model
{
    protected $table = 'email_verify';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'token',
        'revoged',
        'created_at'
    ];

    protected $casts = [
        'revoged' => 'boolean',
        'created_at' => 'datetime'
    ];
}
