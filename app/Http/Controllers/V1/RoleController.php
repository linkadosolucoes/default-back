<?php

namespace App\Http\Controllers\V1;

use App\Models\Role;
use App\Project\ResponseApi;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function index()
    {
        try {
            return ResponseApi::success(Role::orderBy('name', 'asc')->get());
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }
}
