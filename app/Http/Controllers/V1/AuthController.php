<?php

namespace App\Http\Controllers\V1;

use Carbon\Carbon;
use App\Models\User;
use App\Project\Setting;
use App\Models\EmailVerify;
use Illuminate\Support\Str;
use App\Models\AccessToken;
use Illuminate\Http\Request;
use App\Project\ResponseApi;
use App\Models\PasswordReset;
use App\Mail\MailEmailVerify;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Mail\MailRequestNewpassword;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\CreateNewPasswordRequest;
use App\Http\Requests\Auth\EmailVerifyResendRequest;
use App\Http\Requests\Auth\RequestNewPasswordRequest;

class AuthController extends Controller
{
    public function me()
    {
        try {
            $user = User::select('name', 'email')
                ->find(Auth::user()->id)
                ->toArray();

            $shortName = null;
            $initialsName = null;

            $arrName = explode(" ", $user['name']);
            if (sizeof($arrName) === 1) {
                $shortName = $arrName[0];

                $initialsName = strtoupper(
                    substr($arrName[0], 0, 1)
                );
            } else if (sizeof($arrName) >= 2) {
                $shortName =  $arrName[0] . " " . end($arrName);

                $initialsName = strtoupper(
                    substr($arrName[0], 0, 1) . substr(end($arrName), 0, 1)
                );
            }

            $user['short_name'] = $shortName;
            $user['initials_name'] = $initialsName;
            $user['profile_image'] = null;

            return ResponseApi::success($user);
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function login(LoginRequest $request)
    {
        try {
            $credentials = [
                'email' => $request->username,
                'password' => $request->password
            ];

            $user = User::where('email', $credentials['email'])->first();
            if (is_null($user)) {
                return ResponseApi::error(null, "Usuário ou senha incorreto.", 400);
            }

            if (Setting::getSettingEmailVerify()) {
                if (is_null($user->getEmailVerifiedAt())) {
                    DB::transaction(function () use ($user) {
                        $activeTokens = EmailVerify::where('user_id', $user->id)->where('revoged', false);
                        if (boolval($activeTokens->count())) {
                            $activeTokens->update(['revoged' => true]);
                        }

                        $emailVerify = EmailVerify::create([
                            'user_id' => $user->id,
                            'token' => Str::random(200),
                            'revoged' => false,
                            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ]);

                        Mail::to($user->email)->send(new MailEmailVerify($user, $emailVerify));
                    });

                    return ResponseApi::error(null, "Verificação de e-mail pendente. Enviamos um e-mail de verificação.", 401);
                }
            }

            if ($user->isBlocked()) {
                return ResponseApi::error(null, "Usuário bloqueado. Solicite uma nova senha.", 401);
            }

            if ($user->isDisabled()) {
                return ResponseApi::error(null, "Usuário desabilitado.", 401);
            }

            $authenticated = Auth::attempt($credentials);
            if (!$authenticated) {
                return ResponseApi::error(null, "Usuário ou senha incorreto.", 401);
            }

            $jwtToken = JWTAuth::fromUser(Auth::user());

            DB::transaction(function () use ($user, $jwtToken) {
                if (!Setting::getSettingMultipleLogin()) {
                    $this->revogedTokensByUser($user);
                }

                AccessToken::create([
                    'user_id' => $user->id,
                    'token' => $jwtToken,
                    'revoged' => false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            });

            $data = [
                'token_type' => 'Bearer',
                'access_token' => $jwtToken
            ];

            return ResponseApi::success($data);
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function logout(Request $request)
    {
        try {
            $authorization = $request->header('Authorization');
            if (!is_null($authorization)) {
                $arrAccessToken = explode(" ", $authorization);

                $activeTokens = AccessToken::where('token', end($arrAccessToken));
                if (boolval($activeTokens->count())) {
                    $activeTokens->update(['revoged' => true]);
                }
            }

            return ResponseApi::success();
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function emailVerify($token)
    {
        try {
            $dataToken = EmailVerify::where('token', $token)->where('revoged', false)->first();
            if (is_null($dataToken)) {
                return ResponseApi::error(null, "Token de verificação inválido. Solicite um novo e-mail de verificação.", 404);
            }

            $user = User::find($dataToken->user_id);
            if (is_null($user)) {
                $dataToken->revoged = true;
                $dataToken->save();

                return ResponseApi::error(null, "Token de verificação inválido.", 400);
            }

            $now = Carbon::now()->getTimestamp();
            $timeLimit = Carbon::parse($dataToken->created_at)->addMinutes(15)->getTimestamp();
            if ($now > $timeLimit) {
                DB::transaction(function () use ($user) {
                    $activeTokens = EmailVerify::where('user_id', $user->id)->where('revoged', false);
                    if (boolval($activeTokens->count())) {
                        $activeTokens->update(['revoged' => true]);
                    }

                    $emailVerify = EmailVerify::create([
                        'user_id' => $user->id,
                        'token' => Str::random(200),
                        'revoged' => false,
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    Mail::to($user->email)->send(new MailEmailVerify($user, $emailVerify));
                });

                return ResponseApi::error(null, "Token de verificação expirado. Enviamos um novo e-mail de verificação.");
            }

            DB::transaction(function () use ($dataToken, $user) {
                $dataToken->revoged = true;
                $dataToken->save();

                $user->email_verified_at = Carbon::now();
                $user->save();
            });

            return ResponseApi::success();
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function emailVerifyResend(EmailVerifyResendRequest $request)
    {
        try {
            $user = User::where('email', $request->email)->first();
            if (is_null($user)) {
                return ResponseApi::error(null, "Usuário não identificado.", 404);
            }

            if (!is_null($user->getEmailVerifiedAt())) {
                return ResponseApi::error(null, "E-mail já verificado.");
            }

            DB::transaction(function () use ($user) {
                $dataTokens = EmailVerify::where('user_id', $user->id)->where('revoged', false);
                if (boolval($dataTokens->count())) {
                    $dataTokens->update(['revoged' => true]);
                }

                $emailVerify = EmailVerify::create([
                    'user_id' => $user->id,
                    'token' => Str::random(200),
                    'revoged' => false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

                Mail::to($user->email)->send(new MailEmailVerify($user, $emailVerify));
            });

            return ResponseApi::success();
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        try {
            $user = User::find(Auth::user()->id);
            if (is_null($user)) {
                return ResponseApi::error(null, "Usuário não identificado.", 404);
            }

            $currentCredentials = [
                'email' => $user->email,
                'password' => $request->current_password
            ];

            $authenticated = Auth::attempt($currentCredentials);
            if (!$authenticated) {
                return ResponseApi::error(null, "Senha atual inválida.", 401);
            }

            if ($request->new_password !== $request->confirm_new_password) {
                return ResponseApi::error(null, "As senhas informadas não conferem", 401);
            }

            $user->password = bcrypt($request->new_password);
            $user->save();

            return ResponseApi::success();
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function createNewPassword(CreateNewPasswordRequest $request)
    {
        try {
            $dataToken = PasswordReset::where('token', $request->token)->where('revoged', false)->first();
            if (is_null($dataToken)) {
                return ResponseApi::error(null, "Link inválido. Solicite um novo link.", 404);
            }

            $user = User::where('id', $dataToken->user_id)->first();
            if (is_null($user)) {
                return ResponseApi::error(null, "Link inválido. Solicite uma nova link.", 401);
            }

            $now = Carbon::now()->getTimestamp();
            $timeLimit = Carbon::parse($dataToken->created_at)->addMinutes(15)->getTimestamp();
            if ($now > $timeLimit) {
                DB::transaction(function () use ($user) {
                    $dataTokens = PasswordReset::where('user_id', $user->id)->where('revoged', false);
                    if (boolval($dataTokens->count())) {
                        $dataTokens->update(['revoged' => true]);
                    }

                    $passwordReset = PasswordReset::create([
                        'user_id' => $user->id,
                        'token' => Str::random(200),
                        'revoged' => false,
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    $user->blocked = true;
                    $user->save();

                    Mail::to($user->email)->send(new MailRequestNewpassword($user, $passwordReset));
                });

                return ResponseApi::error(null, "Link expirado. Enviamos um novo link para seu e-mail.", 401);
            }

            if ($request->new_password !== $request->confirm_new_password) {
                return ResponseApi::error(null, "As senhas informadas não conferem", 400);
            }

            DB::transaction(function () use ($dataToken, $user, $request) {
                $dataToken->revoged = true;
                $dataToken->save();

                $user->password = bcrypt($request->new_password);
                $user->blocked = false;
                $user->save();
            });

            return ResponseApi::success();
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function requestNewPassword(RequestNewPasswordRequest $request)
    {
        try {
            $user = User::where('email', $request->email)->first();
            if (is_null($user)) {
                return ResponseApi::error(null, "Usuário não identificado.", 404);
            }

            if ($user->isDisabled()) {
                return ResponseApi::error(null, "Usuário desabilitado.", 401);
            }

            if (Setting::getSettingEmailVerify()) {
                if (is_null($user->getEmailVerifiedAt())) {
                    return ResponseApi::error(null, "E-mail não verificado. Realize a verificação do seu e-mail.", 401);
                }
            }

            DB::transaction(function () use ($user) {
                if (!Setting::getSettingMultipleLogin()) {
                    $this->revogedTokensByUser($user);
                }

                $dataTokens = PasswordReset::where('user_id', $user->id)->where('revoged', false);
                if (boolval($dataTokens->count())) {
                    $dataTokens->update(['revoged' => true]);
                }

                $passwordReset = PasswordReset::create([
                    'user_id' => $user->id,
                    'token' => Str::random(200),
                    'revoged' => false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

                $user->blocked = true;
                $user->save();

                Mail::to($user->email)->send(new MailRequestNewpassword($user, $passwordReset));
            });

            return ResponseApi::success();
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    private function revogedTokensByUser(User $user)
    {
        $activeTokens = AccessToken::where('revoged', false)->where('user_id', $user->id);
        if (boolval($activeTokens->count())) {
            $activeTokens->update(['revoged' => true]);
        }
    }
}
