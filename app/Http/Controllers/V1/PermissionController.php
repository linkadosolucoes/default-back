<?php

namespace App\Http\Controllers\V1;

use App\Models\User;
use App\Models\Role;
use App\Project\ResponseApi;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    public function index()
    {
        try {
            $roles = Role::all();

            return ResponseApi::success($roles);
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function permissionsByUser()
    {
        try {
            $data = [];

            $authUser = Auth::user();
            $user = User::find($authUser->id);

            $roles = $user->roles;
            foreach ($roles as $role) {
                foreach ($role->permissions as $permission) {
                    $data[$role->object][] = $permission->object;
                }
            }

            return ResponseApi::success($data);
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }
}
