<?php

namespace App\Http\Middleware;

use App\Models\AccessToken;
use Closure;
use App\Project\ResponseApi;
use Illuminate\Http\Request;

class CheckAccessToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $authorization = $request->header('Authorization');
            if (is_null($authorization)) {
                return ResponseApi::error(null, "Obrigatório enviar o token de acesso.", 401);
            }

            $arrAccessToken = explode(" ", $authorization);

            $accessToken = AccessToken::where('token', end($arrAccessToken))->where('revoged', false)->first();
            if (is_null($accessToken)) {
                return ResponseApi::error(null, "Token de acesso inválido.", 401);
            }

            return $next($request);

        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }
}
