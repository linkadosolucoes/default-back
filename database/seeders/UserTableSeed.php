<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = collect([
            [
                'name' => 'Superadmin',
                'email' => 'superadmin@ncode.dev',
                'email_verified_at' => Carbon::now(),
                'password' => bcrypt(env('PASSWORD_SUPERADMIN', 'superadmin')),
                'blocked' => false,
                'disabled' => false
            ]
        ]);

        $users->each(function ($user) {
            $userExist = User::where('email', $user['email'])->count();
            if (!boolval($userExist)) {
                User::create($user);
            }
        });
    }
}
